# Encrypt/decrypt your file with AES!

usage: `aes_encrypt.py` [-h] filename {enc,dec} passphrase

## Description
### Positional arguments
* `filename`    File to to en/de-crypt
* `{enc,dec}`   Mode: encrypt or decrypt
* `passphrase`  Password to en/de-crypt

### Optional arguments
* `-h`, `--help`  show this help message and exit