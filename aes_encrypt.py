from Crypto.Cipher import AES
from Crypto.Hash import SHA256
import argparse

KEY_SIZE_STR = 32
IV_SIZE_STR = 16


def parser():
	parser = argparse.ArgumentParser(description='Encrypt/decrypt your file with AES!')
	parser.add_argument('filename', type=str, help='File to to en/de-crypt')
	parser.add_argument('mode', choices = ['enc', 'dec'], help='Mode: encrypt or decrypt')
	parser.add_argument('passphrase', type=str, help='Password to en/de-crypt')
	return parser

def main():
	pars = parser()
	args = pars.parse_args()
	passphrase = args.passphrase
	key = SHA256.new()
	key.update(str.encode(passphrase))

	iv = SHA256.new()
	iv.update(b'IsThisIVGood?')

	cipher = AES.new(key.hexdigest()[:KEY_SIZE_STR], AES.MODE_CFB, iv.hexdigest()[:IV_SIZE_STR])
	
	if args.mode == 'enc':
		with open(args.filename, 'rb') as FILE:
			ct = cipher.encrypt(FILE.read())

		with open('{0}.enc'.format(args.filename),'wb') as FILE:
			FILE.write(ct)
	if args.mode == 'dec':
		with open(args.filename, 'rb') as FILE:
			pt = cipher.decrypt(FILE.read())

		with open('{0}.dec'.format(args.filename),'wb') as FILE:
			FILE.write(pt)
if __name__ == "__main__":
	main()


